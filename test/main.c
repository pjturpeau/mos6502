#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include "6502/mos6502.h"

#define PRINTPS(BIT) ((cpu.P.BIT) ? #BIT[0] : tolower(#BIT[0]))

void disasm(u16 pc, char * out) {
    u8 ir = _mem[pc];
    opcode_t op = opcodes[ir];

    if(op.addr_mode == _impl_addr) {
        snprintf(out, 256, "%s", op.name);
    }
    else if(op.addr_mode == _imm_addr) {
        u8 o = _mem[pc+1];
        snprintf(out, 256, "%s #$%02X", op.name, o);
    }
    else if(op.addr_mode == _acc_addr) {
        snprintf(out, 256, "%s A", op.name);
    }
    else if(op.addr_mode == _abs_addr) {
        u16 o = abs_read16(pc+1);
        snprintf(out, 256, "%s $%04X", op.name, o);
    }
    else if(op.addr_mode == _absx_addr) {
        u16 o = abs_read16(pc+1);
        snprintf(out, 256, "%s $%04X,X", op.name, o);
    }
    else if(op.addr_mode == _absy_addr) {
        u16 o = abs_read16(pc+1);
        snprintf(out, 256, "%s $%04X,Y", op.name, o);
    }
    else if(op.addr_mode == _absi_addr) {
        u16 o = abs_read16(pc+1);
        snprintf(out, 256, "%s $(%04X)", op.name, o);
    }
    else if(op.addr_mode == _zp_addr) {
        u8 o = _mem[pc+1];
        u8 v = _mem[o];
        snprintf(out, 256, "%s $%02X (%X)", op.name, o, v);
    }
    else if(op.addr_mode == _zpx_addr) {
        u8 o = _mem[pc+1];
        snprintf(out, 256, "%s $%02X,X", op.name, o);
    }
    else if(op.addr_mode == _zpy_addr) {
        u8 o = _mem[pc+1];
        snprintf(out, 256, "%s $%02X,Y", op.name, o);
    }
    else if(op.addr_mode == _xind_addr) {
        u16 o = abs_read16(pc+1);
        snprintf(out, 256, "%s $(%04X,X)", op.name, o);
    }
    else if(op.addr_mode == _indy_addr) {
        u16 o = abs_read16(pc+1);
        snprintf(out, 256, "%s $(%04X),Y", op.name, o);
    }
    else if(op.addr_mode == _rel_addr) {
        s8 o = bus.read(pc+1);
        snprintf(out, 256, "%s $%02X (%+d)", op.name, LO_BYTE(o), o);
    }
}

void print_registers() {

    if(_mem[0x200] < 42) return;

    int PS = 0;
    PS = cpu.P.N;
    PS = (PS * 10) + cpu.P.V; 
    PS = (PS * 10) + cpu.P.X;
    PS = (PS * 10) + cpu.P.B;
    PS = (PS * 10) + cpu.P.D;
    PS = (PS * 10) + cpu.P.I;
    PS = (PS * 10) + cpu.P.Z;
    PS = (PS * 10) + cpu.P.C;

    char dis_str[256];

    disasm(cpu.PC, dis_str);

    printf("%04X PC:%04X AB:%04X A:%02X X:%02X Y:%02X SP:%02X %s %s  [%02X %c%c%c%c%c%c%c%c %08d] %02X %02X %02X %02X // %s // %d \n",
        cpu.cycles, cpu.PC, cpu.AB, cpu.A, cpu.X, cpu.Y, cpu.SP,
        opcodes[cpu.IR].name, opcodes[cpu.D].name,
        cpu.P.byte, PRINTPS(N), PRINTPS(V), PRINTPS(X), PRINTPS(B), PRINTPS(D), PRINTPS(I), PRINTPS(Z), PRINTPS(C), PS, _mem[cpu.PC], _mem[cpu.PC+1], _mem[cpu.PC+2], _mem[cpu.PC+3],
        dis_str,
        _mem[0x200] // test_case number
    );
}

void main(void)
{
    FILE * f = fopen("6502_functional_test.bin", "rb");
    fread(_mem, (1<<16), 1, f);
    fclose(f);
    
    // FILE * f = fopen("AllSuiteA.bin", "rb");
    // fread(_mem, 48*1024, 1, f);
    // fclose(f);

    /* 6502_functional_test.bin */
    bus.write(RESET_VECTOR_LO, 0x00);
    bus.write(RESET_VECTOR_HI, 0x04);

    /* AllSuiteA.bin */
    // bus.write(RESET_VECTOR_LO, 0x00);
    // bus.write(RESET_VECTOR_HI, 0x40);

    reset_mos6502();

    bool run = true;
    while(run) {
        u16 prev_PC = cpu.PC;
        print_registers();
        step_mos6502();
        run = (cpu.PC != prev_PC);
    }
}