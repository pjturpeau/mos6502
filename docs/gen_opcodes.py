# Pierre-Jean Turpeau - 02/2021

input_file = "opcodes.xls"

import os
import xlrd

book = xlrd.open_workbook(input_file)

sheet = book.sheet_by_name("mos6502")
num_rows = sheet.nrows
cur_row = 5 # skip few lines

addr_mode = {
    "accum"     : "acc",
    "implied"   : "impl",
    "imm"       : "imm",
    "zp"        : "zp",
    "zp,x"      : "zpx",
    "zp,y"      : "zpy",
    "abs"       : "abs",
    "abs,x"     : "absx",
    "abs,y"     : "absy",
    "(abs)"     : "absi",
    "(ind,x)"   : "xind",
    "(ind),y"   : "indy",
    "relative"  : "rel"
}

while cur_row < num_rows:
    row = sheet.row(cur_row)

    op = row[0].value + ","

    addr = addr_mode[row[1].value]+","

    comment = " ";
    if(row[7].value):
        comment = " /* "+row[7].value+" */"

    print("    DEF_6502_OPCODE({0}, {1} {2} {3}){4}".format(
        row[4].value, op.ljust(4),
        addr.ljust(5),
        int(row[6].value),
        comment
    ))
    cur_row = cur_row + 1
