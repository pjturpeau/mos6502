/*
 * 6502/mos6502.h
 * 
 * Very simple MOS 6502 emulator:
 *  - instruction stepped
 *  - cycle accurate with cycle penalties
 * 
 * In the plans (in no specific order):
 *  - BCD support
 *  - interrupts support
 *  - cycle-ticked and/or cycle-stepped
 *  - 65c02 and other derivatives
 *  - AB, DB and RW pinout management (at least...)
 *  - Build a complete system
 * 
 * Emulated pinout (not yet...):
 * 
 *          +-------+
 *   AB0 <--|       |<--> DB0
 *   AB1 <--|       |<--> DB1
 *    .. <--| m6502 |<--> ..
 *  AB15 <--|       |<--> DB7
 *          |       |
 *    RW <--|       |
 *          +-------+
 * 
 * Copyleft (c) Pierre-Jean Turpeau 02/2021
 * <pierrejean AT turpeau DOT net>
 */
#ifndef MOS_6502_H
#define MOS_6502_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

typedef int8_t s8;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef int16_t s16;

#define HI_BYTE(v) ((v >> 8) & 0xFF)
#define LO_BYTE(v) (v & 0xFF)
#define TO_WORD(HI, LO) (((HI & 0xFF) << 8) | (LO & 0xFF))

#define IRQ_BRK_VECTOR_HI   0xFFFF
#define IRQ_BRK_VECTOR_LO   0xFFFE
#define RESET_VECTOR_HI     0xFFFD
#define RESET_VECTOR_LO     0xFFFC
#define NMI_VECTOR_HI       0xFFFB
#define NMI_VECTOR_LO       0xFFFA

#define OVERFLOW_BIT    0x40
#define SIGN_BIT        0x80

typedef union {
    u8 byte;
    struct __attribute__((__packed__)) {
        /* little endian ordering */
        unsigned C : 1; /* carry flag: 1 on unsigned overflow */
        unsigned Z : 1; /* zero flag: 1 when all bits of a result are zero */ 
        unsigned I : 1; /* IRQ flag: when 1 no interrupts, except BRK/NMI */
        unsigned D : 1; /* decimal flag: 1 when CPU in BCD mode */
        unsigned B : 1; /* unused / virtual B flag */ 
        unsigned X : 1; /* unused: always 1 */ 
        unsigned V : 1; /* overflow flag: 1 on signed overflow */
        unsigned N : 1; /* negative flag: 1 when result is negative */
    };
} status_register_t;

typedef struct mos6502 {
    u32 cycles;
    u32 extra_cycles; /* if instruction generate extra-cycles */

    u16 PC; /* Program Counter */
    u16 AB; /* Address Bus register: pinout AB0-AB15 */
    u16 RA; /* Relative Address for branching*/

    u8 A, X, Y;/* Accumulator and indexes */
    u8 SP; /* Stack Pointer: 8 bits wrap-around index in the stack page */
    status_register_t P; /* Processor status register */
    u8 D; // prefetch-Decode register: holds the next pipelined opcde */
    // u8 DB; /* Data Bus register: pinout DB0-DB7 */
    u8 IR; /* Instruction Register holds opcode in execution */

    bool page_crossed;
} mos6502_t;

static mos6502_t cpu;

typedef struct {
    void (*instr)();
    void (*addr_mode)();
    u32 cycles;
    char * name;
}
opcode_t;

opcode_t opcodes[256];

typedef struct {
    /* external bus callbacks can be overloaded for debug purpose */
    u8 (*read)(u16 addr);
    void (*write)(u16 addr, u8 data);
}
bus_device_t;

/* 64 Kb addressable memory
 * Page 00 - 0x0000 to 0x00FF - zero page 
 * Page 01 - 0x0100 to 0x01FF - cpu stage page
 * Page FF - 0xFF00 to 0xFFFF - interrupts vector page */ 
#define BASE_STACK 0x100
static u8 _mem[(1<<16)] = {};
u8 _read(u16 addr) { return _mem[addr];}
void _write(u16 addr, u8 val) { _mem[addr] = val; };

bus_device_t bus = { _read, _write };

/* -------------------------------------------------------------------------
 * Flags, stack and memory R/W utilities
 * ------------------------------------------------------------------------- */

void update_N_flag(u8 value) {
    cpu.P.N = (value & SIGN_BIT) ? 1 : 0;
}

void update_Z_flag(u8 value) {
    cpu.P.Z = (value == 0) ? 1 : 0;
}

void update_V_flag(u8 M, u16 result) {
    cpu.P.V = ((cpu.A ^ result) & (M ^ result) & SIGN_BIT) ? 1 : 0;
}

void push8(u8 value) {
    /* no need to check SP for out-of-bound value since it's a 8 bits register
     * which should naturally wrap-around */
    bus.write(BASE_STACK | cpu.SP, value);
    cpu.SP--;
}

u8 pop8() {
    /* no need to check SP for out-of-bound value since it's a 8 bits register
     * which should naturally wrap-around */
    return bus.read(BASE_STACK | (++cpu.SP));
}

void push16(u16 value) {
    /* no need to check SP for out-of-bound value since it's a 8 bits register
     * which should naturally wrap-around */
    bus.write(BASE_STACK | cpu.SP, HI_BYTE(value));
    bus.write(BASE_STACK | (cpu.SP - 1), LO_BYTE(value));
    cpu.SP -= 2;
}

u16 pop16() {
    /* no need to check SP for out-of-bound value since it's a 8 bits register
     * which should naturally wrap-around */
    cpu.SP += 2;
    return TO_WORD(bus.read(BASE_STACK + cpu.SP),
        bus.read(BASE_STACK + ((cpu.SP - 1) & 0xFF)));
}

/* read 16 bit value from zero-page with wrap-around */
u16 zp_read16(u8 zp_addr) {
    return TO_WORD(bus.read((zp_addr+1) & 0xFF), bus.read(zp_addr));
}

/* read 16 bit value from absolute address */
u16 abs_read16(u16 addr) {
    return TO_WORD(bus.read(addr+1), bus.read(addr));
}

bool has_page_crossed(u16 from, u16 to) {
    return ((from & 0xFF00) != (to & 0xFF00)) ? true : false;
}

/* -------------------------------------------------------------------------
 * External control functions
 * ------------------------------------------------------------------------- */

void load_rom(u16 address, const u8 * bytes, u32 size) {
    if(address < 0x200 || address > 0xFF00)
        return;
    for(int i = 0; i < size; i++)
        bus.write(address + i, bytes[i]);
    bus.write(RESET_VECTOR_LO, LO_BYTE(address));
    bus.write(RESET_VECTOR_HI, HI_BYTE(address));
}

static void def_opcodes(); /* forward decl. */

void reset_mos6502() {
    def_opcodes();

    cpu.AB = RESET_VECTOR_LO;
    cpu.PC = abs_read16(cpu.AB);
    cpu.A = cpu.X = cpu.Y = 0;
    cpu.P.byte = 0;
    cpu.P.X = 1;

    /* reset sequence is like BRK/IRQ starting with SP = 0
     * push(PC) => SP=0xFE, push(P) => SP=0xFD */
    cpu.SP = 0xFD; 

    /* pre-load next opcode */
    cpu.D = bus.read(cpu.PC);

    cpu.cycles = 0;
}

int step_mos6502() {
    cpu.IR = bus.read(cpu.PC++);
    opcode_t opc = opcodes[cpu.IR];

    cpu.extra_cycles = 0;

    opc.addr_mode();
    opc.instr();

    cpu.cycles += opc.cycles + cpu.extra_cycles;

    cpu.D = bus.read(cpu.PC);

    return opc.cycles + cpu.extra_cycles;
}

/* -------------------------------------------------------------------------
 * Operations for addressing modes
 * ------------------------------------------------------------------------- */

/* direct accumulator addressing */
static void _acc_addr() {
    return;
}

/* implied addressing */
static void _impl_addr() {
    return;
}

/* immediate addressing */
static void _imm_addr() {
    cpu.AB = cpu.PC;
    cpu.PC++;
}

static void _zp_addr() {
    cpu.AB = bus.read(cpu.PC); 
    cpu.PC++; 
}

void _zpx_addr() {
    cpu.AB = (bus.read(cpu.PC) + cpu.X) & 0xFF; /* zero-page wrapping */
    cpu.PC++;
}

void _zpy_addr() {
    cpu.AB = (bus.read(cpu.PC) + cpu.Y) & 0xFF; /* zero-page wrapping */
    cpu.PC++;
}

static void _abs_addr() {
    cpu.AB = abs_read16(cpu.PC);
    cpu.PC += 2;
}

void _absx_addr() {
    cpu.AB = abs_read16(cpu.PC);
    u16 tmp = cpu.AB + cpu.X;
    cpu.page_crossed = has_page_crossed(cpu.AB, tmp);
    cpu.AB = tmp;
    cpu.PC += 2;
}

void _absy_addr() {
    cpu.AB = abs_read16(cpu.PC);
    u16 tmp = cpu.AB + cpu.Y;
    cpu.page_crossed = has_page_crossed(cpu.AB, tmp);
    cpu.AB = tmp;
    cpu.PC += 2;    
}

/* absolute indirect */
void _absi_addr() {
    cpu.AB = abs_read16(cpu.PC);

    /* Emulate JMP bug with lSB-byte wrap-around without carry on the MSB */ 
    u16 addr_bug = (cpu.AB & 0xFF00) | ((cpu.AB + 1) & 0xFF);
    cpu.AB = TO_WORD(bus.read(addr_bug), bus.read(cpu.AB));

    /* Sholl be: cpu.AB = abs_read16(cpu.AB); (e.g., fixed on 65c02) */

    cpu.PC += 2;
}

/* indexed indirect */
void _xind_addr() {
    u8 zp_addr = (bus.read(cpu.PC) + cpu.X) & 0xFF; /* zero-page wrapping */
    cpu.PC++;
    cpu.AB = zp_read16(zp_addr);
}        

/* indirect index */
void _indy_addr() {
    u8 zp_addr = bus.read(cpu.PC);
    cpu.PC++;
    u16 tmp = zp_read16(zp_addr);
    cpu.AB = tmp + cpu.Y;
    cpu.page_crossed = has_page_crossed(tmp, cpu.AB);
}

/* relative addressing */
void _rel_addr() {
    s16 offset = bus.read(cpu.PC++);

   /* if offset is negative, add sign bits on the MSB of the 16 bits word */
    if(offset & SIGN_BIT) offset |= 0xFF00;

    /* now we can safely add to PC */
    cpu.RA = cpu.PC + offset;
}

/* -------------------------------------------------------------------------
 * 6502 instructions
 * ------------------------------------------------------------------------- */

void _illegal() {
    printf("illegal instruction: %X\n", cpu.IR);
    exit(0);
}

/* ADd memory to accumulator with Carry: A + M + C -> A, C */
void _adc() {
    /* TODO: decimal mode */
    u8 M = bus.read(cpu.AB);
    
    u16 result = M + cpu.A + cpu.P.C;

    cpu.P.C = (result & 0xFF00) ? 1 : 0;
    
    update_V_flag(M, result); /* do not move after setting A register... */
    
    cpu.A = LO_BYTE(result);

    update_Z_flag(cpu.A);
    update_N_flag(cpu.A);
 
    if(cpu.page_crossed) cpu.extra_cycles++;
}

/* AND memory with accumulator: A AND M -> A */
void _and() {
    u8 M = bus.read(cpu.AB);
    M = M & cpu.A;
    update_N_flag(M);
    update_Z_flag(M);
    cpu.A = M;
    if(cpu.page_crossed) cpu.extra_cycles++;
}

/* Arithmetic Shift left one bit on memory */
void _asl() {
    u8 value = bus.read(cpu.AB);
    cpu.P.C = ((value & SIGN_BIT) == SIGN_BIT);
    value = value << 1;
    update_Z_flag(value);
    update_N_flag(value);
    bus.write(cpu.AB, value);
}

/* ASL on Accumulator */
void _asl_acc() {
    u8 value = cpu.A;
    cpu.P.C = ((value & SIGN_BIT) == SIGN_BIT);
    value = value << 1;
    update_Z_flag(value);
    update_N_flag(value);
    cpu.A = value;
}

/* Branch on Carry Clear: branch on C = 0 */
void _bcc() {
    if(!cpu.P.C) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* Branch on Carry Set: branch on C = 1 */
void _bcs() {
    if(cpu.P.C) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* Branch on EQual: branch on Z = 1 */
void _beq() {
    if(cpu.P.Z) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* test BITs in memory with accumulator */
void _bit() {
    u8 M = bus.read(cpu.AB);
    u8 value = cpu.A & M;
    cpu.P.V = (M & OVERFLOW_BIT) ? 1 : 0;
    update_N_flag(M);
    update_Z_flag(value);
}

/* Branch on result MInus: branch on N = 1 */
void _bmi() {
    if(cpu.P.N) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* Branch on Not Equal: branch on Z = 0 */
void _bne() {
    if(!cpu.P.Z) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* Branch on PLus : branch on N = 0 */
void _bpl() {
    if(!cpu.P.N) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* BReaK */
void _brk() {
    cpu.PC += 1; /* one extra byte is saved after BRK for debug purpose */
    push16(cpu.PC);
    status_register_t P = cpu.P;
    P.B = 1; /* virtual B flag */
    push8(P.byte);
    cpu.P.I = 1;
    cpu.AB = IRQ_BRK_VECTOR_LO;
    cpu.PC = abs_read16(cpu.AB);
}

/* Branch on oVerflow Clear: branch on V = 0 */
void _bvc() {
    if(!cpu.P.V) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* Branch on oVerflow Set: branch on V = 1 */
void _bvs() {
    if(cpu.P.V) {
        cpu.extra_cycles += has_page_crossed(cpu.PC, cpu.RA) ? 2 : 1;
        cpu.PC = cpu.RA;
    }
}

/* CLear Carry : 0 -> C */
void _clc() {
    cpu.P.C = 0;
}

/* CLear Decimal mode: 0 -> D */
void _cld() {
    cpu.P.D = 0;
}

/* CLear Interrupt disable flag: 0 -> I */
void _cli() {
    cpu.P.I = 0;
}

/* Clear oVerflow flag: 0 -> V */
void _clv() {
    cpu.P.V = 0;
}

/* Compare Memory with Accumulator: A - M */
void _cmp() {
    u8 M = bus.read(cpu.AB);
    
    // bool tr = (cpu.AB == 0x233) || (cpu.AB == 0x223) || (cpu.AB == 0xF);

    u8 result = cpu.A - M;
    // if(tr) printf("trace: A:%d, M:%d, r:%x\n", cpu.A, M, result);
    cpu.P.C = (cpu.A >= M) ? 1 : 0;
    update_Z_flag(result);
    update_N_flag(result);
}

/* Compare Memory with index X: X - M */
void _cpx() {
    u8 M = bus.read(cpu.AB);
    u8 result = cpu.X - M;
    cpu.P.C = (cpu.X >= M) ? 1 : 0;
    update_Z_flag(result);
    update_N_flag(result);
}

/* Compare Memory with index Y: Y - M */
void _cpy() {
    u8 M = bus.read(cpu.AB);
    u8 result = cpu.Y - M;
    cpu.P.C = (cpu.Y >= M) ? 1 : 0;
    update_Z_flag(result);
    update_N_flag(result);
}

/* DECrement memory by one: M - 1 -> M */
void _dec() {
    u8 M = bus.read(cpu.AB) - 1;
    update_N_flag(M);
    update_Z_flag(M);
    bus.write(cpu.AB, M);
}

/* DEcrement index Y by one: X - 1 -> X */
void _dex() {
    cpu.X --;
    update_N_flag(cpu.X);
    update_Z_flag(cpu.X);
}

/* DEcrement index Y by one: Y - 1 -> Y */
void _dey() {
    cpu.Y --;
    update_N_flag(cpu.Y);
    update_Z_flag(cpu.Y);
}

/* Exclusive OR memory with accumulator: A EOR M -> A */
void _eor() {
    cpu.A = cpu.A ^ bus.read(cpu.AB);
    update_N_flag(cpu.A);
    update_Z_flag(cpu.A);
    if(cpu.page_crossed)
        cpu.extra_cycles++; 
}

/* INCrement memory by one: M + 1 -> M */
void _inc() {
    u8 M = bus.read(cpu.AB) + 1;
    update_N_flag(M);
    update_Z_flag(M);
    bus.write(cpu.AB, M);
}

/* INcrement X by one: X + 1 -> X */
void _inx() {
    cpu.X++;
    update_N_flag(cpu.X);
    update_Z_flag(cpu.X);
}

/* INcrement Y by one: Y + 1 -> Y */
void _iny() {
    cpu.Y++;
    update_N_flag(cpu.Y);
    update_Z_flag(cpu.Y);
}

/* JuMP to new location */
void _jmp() {
    cpu.PC = cpu.AB;
}

/* Jump to new location Saving Return address */
void _jsr() {
    push16(cpu.PC - 1); /* because we have +1 on RTS... */
    cpu.PC = cpu.AB;
}

/* LoaD Accumulator with memory: M -> A */
void _lda() {
    cpu.A = bus.read(cpu.AB);
    update_N_flag(cpu.A);
    update_Z_flag(cpu.A);
    if(cpu.page_crossed) cpu.extra_cycles++;
}

/* LoaD index X with memory: M -> X */
void _ldx() {
    cpu.X = bus.read(cpu.AB);
    update_N_flag(cpu.X);
    update_Z_flag(cpu.X);
    if(cpu.page_crossed) cpu.extra_cycles++;
}

/* LoaD index Y with memory: M -> Y */
void _ldy() {
    cpu.Y = bus.read(cpu.AB);
    update_N_flag(cpu.Y);
    update_Z_flag(cpu.Y);
    if(cpu.page_crossed) cpu.extra_cycles++;
}

/* Logical Shift one bit Right */
void _lsr() {
    u8 value = bus.read(cpu.AB);
    cpu.P.C = value & 1;
    value >>= 1;
    cpu.P.N = 0;
    update_Z_flag(value);
    bus.write(cpu.AB, value);
}

/* Logical Shift one bit Right (accumulator) */
void _lsr_acc() {
    cpu.P.C = cpu.A & 1;
    cpu.A >>= 1;
    cpu.P.N = 0;
    update_Z_flag(cpu.A);
}

/* No OPeration */
void _nop() {
    /* nothing to do */
}

/* OR memory with Accumulator: A OR M -> A */
void _ora() {
    u8 m = bus.read(cpu.AB);
    cpu.A = cpu.A | m;
    update_N_flag(cpu.A);
    update_Z_flag(cpu.A);
    if(cpu.page_crossed) cpu.extra_cycles++;
}

/* PusH Accumulator on stack */
void _pha() {
    push8(cpu.A);
}

/* PusH Processor status on stack */
void _php() {
    status_register_t P = cpu.P;
    P.B = 1; /* seems to be a PHP bug */
    push8(P.byte);
}

/* PuLl Accumulator from stack */
void _pla() {
    cpu.A = pop8();
    update_Z_flag(cpu.A);
    update_N_flag(cpu.A);
}

/* PuLl Processor status from stack */
void _plp() {
    cpu.P.byte = pop8();
    cpu.P.X = 1; /* always set... */
    cpu.P.B = 1; /* always set also... */
}

/* Rotate One bit Left (memory) */
void _rol() {
    u8 M = bus.read(cpu.AB);
    u8 result = (M << 1) | cpu.P.C;
    cpu.P.C = (M & SIGN_BIT) ? 1 : 0;
    update_Z_flag(result);
    update_N_flag(result);
    bus.write(cpu.AB, result);
}

/* Rotate One bit Left (accumulator) */
void _rol_acc() {
    u8 result = (cpu.A << 1) | cpu.P.C;
    cpu.P.C = (cpu.A & SIGN_BIT) ? 1 : 0;
    update_Z_flag(result);
    update_N_flag(result);
    cpu.A = result;
}

/* Rotate One bit Right */
void _ror() {
    u8 M = bus.read(cpu.AB);
    u8 result = (M >> 1) | (cpu.P.C << 7);
    cpu.P.C = (M & 1) ? 1 : 0;
    update_Z_flag(result);
    update_N_flag(result);
    bus.write(cpu.AB, result);
}

/* Rotate One bit Right */
void _ror_acc() {
    u8 result = (cpu.A >> 1) | (cpu.P.C << 7);

    cpu.P.C = (cpu.A & 1) ? 1 : 0;
    cpu.A = result;
    update_Z_flag(cpu.A);
    update_N_flag(cpu.A);
}

/* ReTurn from Interrupt: pull SR, pull PC */
void _rti() {
    cpu.P.byte = pop8();
    cpu.PC = pop16();
}

/* ReTurn from Subroutine: pull PC, PC+1 -> PC */
void _rts() {
    cpu.PC = pop16() + 1;
}

/* Subtract with Borrowing and Carrying: A - M - C -> A */
void _sbc() {
    u8 M = bus.read(cpu.AB) ^ 0xFFU;

    u16 result = cpu.A + M + cpu.P.C;

    cpu.P.C = (result & 0xFF00U) ? 1 : 0;

    update_V_flag(M, result); /* update before setting the A register! */

    cpu.A = LO_BYTE(result);
    update_Z_flag(cpu.A);
    update_N_flag(cpu.A);

    if(cpu.page_crossed) cpu.extra_cycles++;
}

/* SEt Carry flag: 1 -> C */
void _sec() {
    cpu.P.C = 1;
}

/* SEt Decimal mode: 1 -> D */
void _sed() {
    cpu.P.D = 1;
}

/* SEt Interrupt disable status: 1 -> I */
void _sei() {
    cpu.P.I = 1;
}

/* STore Accumulator in memory: A -> M */
void _sta() {
    bus.write(cpu.AB, cpu.A);
}

/* STore index X in memory: X -> M */
void _stx() {
    bus.write(cpu.AB, cpu.X);
}

/* STore index Y in memory: Y -> M */
void _sty() {
    bus.write(cpu.AB, cpu.Y);
}

/* Transfer Accumulator to index X: A -> X */
void _tax() {
    cpu.X = cpu.A;
    update_N_flag(cpu.X);
    update_Z_flag(cpu.X);
}

/* Transfer Accumulator to index Y: A -> Y */
void _tay() {
    cpu.Y = cpu.A;
    update_N_flag(cpu.Y);
    update_Z_flag(cpu.Y);
}

/* Transfer Stack pointer to index X: SP -> X */
void _tsx() {
    cpu.X = cpu.SP;
    update_N_flag(cpu.X);
    update_Z_flag(cpu.X);
}

/* Transfer index X to Accumulator: X -> A */
void _txa() {
    cpu.A = cpu.X;
    update_Z_flag(cpu.A);
    update_N_flag(cpu.A);
}

/* Transfer index X to Stack pointer: X -> S */
void _txs() {
    cpu.SP = cpu.X;
}

/* Transfer index Y to Accumulator: Y -> A */
void _tya() {
    cpu.A = cpu.Y;
    update_Z_flag(cpu.A);
    update_N_flag(cpu.A);
}

/* -------------------------------------------------------------------------
 * Opcodes definition
 * ------------------------------------------------------------------------- */

#define DEF_6502_imm
#define DEF_6502_impl
#define DEF_6502_acc(X) X##_acc /* trick for accumulator based instructions */
#define DEF_6502_xind
#define DEF_6502_indy
#define DEF_6502_zp
#define DEF_6502_zpx
#define DEF_6502_zpy
#define DEF_6502_abs
#define DEF_6502_absx
#define DEF_6502_absy
#define DEF_6502_absi
#define DEF_6502_rel

#define DEF_6502_OPCODE(IDX, NAME, ADDRESSING, CYCLES) {\
    opcodes[IDX].instr = DEF_6502_##ADDRESSING(_##NAME); \
    opcodes[IDX].addr_mode = _##ADDRESSING##_addr; \
    opcodes[IDX].cycles = CYCLES; \
    opcodes[IDX].name = #NAME; }

#define DEF_6502_ILLEGAL(IDX) DEF_6502_OPCODE(IDX, illegal, impl, 2)

static void def_opcodes() {
    for(int i = 0; i < 256; i++) DEF_6502_ILLEGAL(i);
    DEF_6502_OPCODE(0x00, brk, impl, 7)
    DEF_6502_OPCODE(0x01, ora, xind, 6)
    DEF_6502_OPCODE(0x05, ora, zp,   3)
    DEF_6502_OPCODE(0x06, asl, zp,   5)
    DEF_6502_OPCODE(0x08, php, impl, 3)
    DEF_6502_OPCODE(0x09, ora, imm,  2)
    DEF_6502_OPCODE(0x0a, asl, acc,  2)
    DEF_6502_OPCODE(0x0d, ora, abs,  4)
    DEF_6502_OPCODE(0x0e, asl, abs,  6)
    DEF_6502_OPCODE(0x10, bpl, rel,  2) /* BR */
    DEF_6502_OPCODE(0x11, ora, indy, 5) /* PB */
    DEF_6502_OPCODE(0x15, ora, zpx,  4)
    DEF_6502_OPCODE(0x16, asl, zpx,  6)
    DEF_6502_OPCODE(0x18, clc, impl, 2)
    DEF_6502_OPCODE(0x19, ora, absy, 4) /* PB */
    DEF_6502_OPCODE(0x1d, ora, absx, 4) /* PB */
    DEF_6502_OPCODE(0x1e, asl, absx, 7)
    DEF_6502_OPCODE(0x20, jsr, abs,  6)
    DEF_6502_OPCODE(0x21, and, xind, 6)
    DEF_6502_OPCODE(0x24, bit, zp,   3)
    DEF_6502_OPCODE(0x25, and, zp,   3)
    DEF_6502_OPCODE(0x26, rol, zp,   5)
    DEF_6502_OPCODE(0x28, plp, impl, 4)
    DEF_6502_OPCODE(0x29, and, imm,  2)
    DEF_6502_OPCODE(0x2a, rol, acc,  2)
    DEF_6502_OPCODE(0x2c, bit, abs,  4)
    DEF_6502_OPCODE(0x2d, and, abs,  4)
    DEF_6502_OPCODE(0x2e, rol, abs,  6)
    DEF_6502_OPCODE(0x30, bmi, rel,  2) /* BR */
    DEF_6502_OPCODE(0x31, and, indy, 5) /* PB */
    DEF_6502_OPCODE(0x35, and, zpx,  4)
    DEF_6502_OPCODE(0x36, rol, zpx,  6)
    DEF_6502_OPCODE(0x38, sec, impl, 2)
    DEF_6502_OPCODE(0x39, and, absy, 4) /* PB */
    DEF_6502_OPCODE(0x3d, and, absx, 4) /* PB */
    DEF_6502_OPCODE(0x3e, rol, absx, 7)
    DEF_6502_OPCODE(0x40, rti, impl, 6)
    DEF_6502_OPCODE(0x41, eor, xind, 6)
    DEF_6502_OPCODE(0x45, eor, zp,   3)
    DEF_6502_OPCODE(0x46, lsr, zp,   5)
    DEF_6502_OPCODE(0x48, pha, impl, 3)
    DEF_6502_OPCODE(0x49, eor, imm,  2)
    DEF_6502_OPCODE(0x4a, lsr, acc,  2)
    DEF_6502_OPCODE(0x4c, jmp, abs,  3)
    DEF_6502_OPCODE(0x4d, eor, abs,  4)
    DEF_6502_OPCODE(0x4e, lsr, abs,  6)
    DEF_6502_OPCODE(0x50, bvc, rel,  2) /* BR */
    DEF_6502_OPCODE(0x51, eor, indy, 5) /* PB */
    DEF_6502_OPCODE(0x55, eor, zpx,  4)
    DEF_6502_OPCODE(0x56, lsr, zpx,  6)
    DEF_6502_OPCODE(0x58, cli, impl, 2)
    DEF_6502_OPCODE(0x59, eor, absy, 4) /* PB */
    DEF_6502_OPCODE(0x5d, eor, absx, 4) /* PB */
    DEF_6502_OPCODE(0x5e, lsr, absx, 7)
    DEF_6502_OPCODE(0x60, rts, impl, 6)
    DEF_6502_OPCODE(0x61, adc, xind, 6) /* DM */
    DEF_6502_OPCODE(0x65, adc, zp,   3) /* DM */
    DEF_6502_OPCODE(0x66, ror, zp,   5)
    DEF_6502_OPCODE(0x68, pla, impl, 4)
    DEF_6502_OPCODE(0x69, adc, imm,  2) /* DM */
    DEF_6502_OPCODE(0x6a, ror, acc,  2)
    DEF_6502_OPCODE(0x6c, jmp, absi, 6)
    DEF_6502_OPCODE(0x6d, adc, abs,  4) /* DM */
    DEF_6502_OPCODE(0x6e, ror, abs,  6)
    DEF_6502_OPCODE(0x70, bvs, rel,  2) /* BR */
    DEF_6502_OPCODE(0x71, adc, indy, 5) /* PB, DM */
    DEF_6502_OPCODE(0x75, adc, zpx,  4) /* DM */
    DEF_6502_OPCODE(0x76, ror, zpx,  6)
    DEF_6502_OPCODE(0x78, sei, impl, 2)
    DEF_6502_OPCODE(0x79, adc, absy, 4) /* PB, DM */
    DEF_6502_OPCODE(0x7d, adc, absx, 4) /* PB, DM */
    DEF_6502_OPCODE(0x7e, ror, absx, 7)
    DEF_6502_OPCODE(0x81, sta, xind, 6)
    DEF_6502_OPCODE(0x84, sty, zp,   3)
    DEF_6502_OPCODE(0x85, sta, zp,   3)
    DEF_6502_OPCODE(0x86, stx, zp,   3)
    DEF_6502_OPCODE(0x88, dey, impl, 2)
    DEF_6502_OPCODE(0x8a, txa, impl, 2)
    DEF_6502_OPCODE(0x8c, sty, abs,  4)
    DEF_6502_OPCODE(0x8d, sta, abs,  4)
    DEF_6502_OPCODE(0x8e, stx, abs,  4)
    DEF_6502_OPCODE(0x90, bcc, rel,  2) /* BR */
    DEF_6502_OPCODE(0x91, sta, indy, 6)
    DEF_6502_OPCODE(0x94, sty, zpx,  4)
    DEF_6502_OPCODE(0x95, sta, zpx,  4)
    DEF_6502_OPCODE(0x96, stx, zpy,  4)
    DEF_6502_OPCODE(0x98, tya, impl, 2)
    DEF_6502_OPCODE(0x99, sta, absy, 5)
    DEF_6502_OPCODE(0x9a, txs, impl, 2)
    DEF_6502_OPCODE(0x9d, sta, absx, 5)
    DEF_6502_OPCODE(0xa0, ldy, imm,  2)
    DEF_6502_OPCODE(0xa1, lda, xind, 6)
    DEF_6502_OPCODE(0xa2, ldx, imm,  2)
    DEF_6502_OPCODE(0xa4, ldy, zp,   3)
    DEF_6502_OPCODE(0xa5, lda, zp,   3)
    DEF_6502_OPCODE(0xa6, ldx, zp,   3)
    DEF_6502_OPCODE(0xa8, tay, impl, 2)
    DEF_6502_OPCODE(0xa9, lda, imm,  2)
    DEF_6502_OPCODE(0xaa, tax, impl, 2)
    DEF_6502_OPCODE(0xac, ldy, abs,  4)
    DEF_6502_OPCODE(0xad, lda, abs,  4)
    DEF_6502_OPCODE(0xae, ldx, abs,  4)
    DEF_6502_OPCODE(0xb0, bcs, rel,  2) /* BR */
    DEF_6502_OPCODE(0xb1, lda, indy, 5) /* PB */
    DEF_6502_OPCODE(0xb4, ldy, zpx,  4)
    DEF_6502_OPCODE(0xb5, lda, zpx,  4)
    DEF_6502_OPCODE(0xb6, ldx, zpy,  4)
    DEF_6502_OPCODE(0xb8, clv, impl, 2)
    DEF_6502_OPCODE(0xb9, lda, absy, 4) /* PB */
    DEF_6502_OPCODE(0xba, tsx, impl, 2)
    DEF_6502_OPCODE(0xbc, ldy, absx, 4) /* PB */
    DEF_6502_OPCODE(0xbd, lda, absx, 4) /* PB */
    DEF_6502_OPCODE(0xbe, ldx, absy, 4) /* PB */
    DEF_6502_OPCODE(0xc0, cpy, imm,  2)
    DEF_6502_OPCODE(0xc1, cmp, xind, 6)
    DEF_6502_OPCODE(0xc4, cpy, zp,   3)
    DEF_6502_OPCODE(0xc5, cmp, zp,   3)
    DEF_6502_OPCODE(0xc6, dec, zp,   5)
    DEF_6502_OPCODE(0xc8, iny, impl, 2)
    DEF_6502_OPCODE(0xc9, cmp, imm,  2)
    DEF_6502_OPCODE(0xca, dex, impl, 2)
    DEF_6502_OPCODE(0xcc, cpy, abs,  4)
    DEF_6502_OPCODE(0xcd, cmp, abs,  4)
    DEF_6502_OPCODE(0xce, dec, abs,  6)
    DEF_6502_OPCODE(0xd0, bne, rel,  2) /* BR */
    DEF_6502_OPCODE(0xd1, cmp, indy, 5) /* PB */
    DEF_6502_OPCODE(0xd5, cmp, zpx,  4)
    DEF_6502_OPCODE(0xd6, dec, zpx,  6)
    DEF_6502_OPCODE(0xd8, cld, impl, 2)
    DEF_6502_OPCODE(0xd9, cmp, absy, 4) /* PB */
    DEF_6502_OPCODE(0xdd, cmp, absx, 4) /* PB */
    DEF_6502_OPCODE(0xde, dec, absx, 7)
    DEF_6502_OPCODE(0xe0, cpx, imm,  2)
    DEF_6502_OPCODE(0xe1, sbc, xind, 6) /* DM */
    DEF_6502_OPCODE(0xe4, cpx, zp,   3)
    DEF_6502_OPCODE(0xe5, sbc, zp,   3) /* DM */
    DEF_6502_OPCODE(0xe6, inc, zp,   5)
    DEF_6502_OPCODE(0xe8, inx, impl, 2)
    DEF_6502_OPCODE(0xe9, sbc, imm,  2) /* DM */
    DEF_6502_OPCODE(0xea, nop, impl, 2)
    DEF_6502_OPCODE(0xec, cpx, abs,  4)
    DEF_6502_OPCODE(0xed, sbc, abs,  4) /* DM */
    DEF_6502_OPCODE(0xee, inc, abs,  6)
    DEF_6502_OPCODE(0xf0, beq, rel,  2) /* BR */
    DEF_6502_OPCODE(0xf1, sbc, indy, 5) /* PB, DM */
    DEF_6502_OPCODE(0xf5, sbc, zpx,  4) /* DM */
    DEF_6502_OPCODE(0xf6, inc, zpx,  6)
    DEF_6502_OPCODE(0xf8, sed, impl, 2)
    DEF_6502_OPCODE(0xf9, sbc, absy, 4) /* PB, DM */
    DEF_6502_OPCODE(0xfd, sbc, absx, 4) /* PB, DM */
    DEF_6502_OPCODE(0xfe, inc, absx, 7)
}

#undef DEF_6502_OPCODE
#undef DEF_6502_ILLEGAL
#undef DEF_6502_imm
#undef DEF_6502_impl
#undef DEF_6502_acc
#undef DEF_6502_xind
#undef DEF_6502_indy
#undef DEF_6502_zp
#undef DEF_6502_zpx
#undef DEF_6502_zpy
#undef DEF_6502_abs
#undef DEF_6502_absx
#undef DEF_6502_absy
#undef DEF_6502_absi
#undef DEF_6502_rel

#endif /* MOS_6502_H */
