# Yet another tiny 6502 emulator

This very simple emulator is my attempt to learn a bit about the MOS 6502 CPU architecture.

A first main goal is to make it pass some 6502 test suites and then later run EhBASIC.

In its primary form, it is a cycle accurate instruction-stepped emulator.
Currently, there's no way to have tick-level callback, nor to have-cycle stepped execution yet. As such it isn't meant to be pin-connected to other microprocessors to build and simulate a complete system such as C64 or NES.

Testing:
 - Using [Klaus Dormann's test suite](https://github.com/Klaus2m5/6502_65C02_functional_tests)
 - Need to implement BCD operations...
 
Next steps (in no specific order):
 - pass functional tests
 - complete tests with BCD, interrupts and unofficial opcodes
 - look at 6502 known bugs or unexpected behaviors
 - clarify external (Address Bus / Data Bus) and internal buses (ADL/ADH, DB, SB) when needed
 - tick-level callbacks to connect the CPU to external BUS management
 - may be a cycle step execution
 - Emulate simple systems such as Apple I/II, may be BBC

## Pictures and schematics

![](images/MOS_6502AD_4585_top.jpg)

![](images/6502CPU.gif)

![](images/6502Chip.gif)

## Links

### Interesting 6502 references

 - [Adressing modes](http://www.emulator101.com/6502-addressing-modes.html)
 - [Instruction set](https://www.masswerk.at/6502/6502_instruction_set.html)
 - [Additional infos on opcodes](http://www.oxyron.de/html/opcodes02.htm)
 - [Internals of BRK/IRQ/NMI/RESET on a MOS 6502](https://www.pagetable.com/?p=410)
 - [Overflow flag explained](http://www.righto.com/2012/12/the-6502-overflow-flag-explained.html)

### Going further in emulation

 - [6502 emulation challenge](https://codegolf.stackexchange.com/questions/12844/emulate-a-mos-6502-cpu)
 - [Cycle-stepped design for cycle-accurate emulation](https://floooh.github.io/2019/12/13/cycle-stepped-6502.html)
 - [Cycles steps for each instruction](http://nesdev.com/6502_cpu.txt) ([local copy](docs/6502_cpu.txt)) (to check with Visual 6502)
 - [Android emulator from scratch](http://androidemufromscratch.blogspot.com/)

### Testing
 
 - [Visual Transistor-level Simulation of the 6502 CPU](http://visual6502.org/)
 - [Perfect6502 : good for parallel testing](https://github.com/mist64/perfect6502)

### Learn 6502 programming

 - [Easy 6502](https://skilldrick.github.io/easy6502/)
 - [Old Skool Coder Tutorials](https://oldskoolcoder.co.uk/tutorials/)

### Tools

 - [CBM .prg Studio IDE](https://www.ajordison.co.uk/)
 - [C64 Studio](https://www.georg-rottensteiner.de/en/index.html)
 - [Relaunch64 IDE](http://www.popelganda.de/relaunch64.html)


